export default [
  {
    name: "foxModel",
    type: "gltfModel",
    path: "models/Fox/glTF/Fox.gltf",
  },
  {
    name: "cubeModel",
    type: "gltfModel",
    path: "models/cube.glb",
  },
];
