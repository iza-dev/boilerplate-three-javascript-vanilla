import Prefab from "../Prefab.js";
import Environment from "./Environment.js";
import Cube from "./Cube.js";

export default class World {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.resources.on("ready", () => {
      this.cube = new Cube();
      this.environment = new Environment();
    });
  }
}
