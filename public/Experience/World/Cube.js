import * as THREE from "three";
import Prefab from "../Prefab.js";

export default class Cube {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;
    this.time = this.prefab.time;

    this.resource = this.resources.items.cubeModel;

    this.setModel();
  }

  setModel() {
    this.model = this.resource.scene;
    this.model.scale.set(0.02, 0.02, 0.02);
    this.scene.add(this.model);

    this.model.traverse((child) => {
      if (child instanceof THREE.Mesh) child.castShadow = true;
    });
  }
}
